package fr.d4rt4gnan.openqia.company.technical.resolvers;


import fr.d4rt4gnan.openqia.company.technical.mappers.CompanyMapper;
import fr.d4rt4gnan.openqia.company.technical.mappers.impl.CompanyMapperImpl;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
public class CompanyMapperParameterResolver implements ParameterResolver {
    
    @Override
    public boolean supportsParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == CompanyMapper.class;
    }
    
    @Override
    public Object resolveParameter (
            ParameterContext parameterContext, ExtensionContext extensionContext
    ) throws ParameterResolutionException {
        return new CompanyMapperImpl();
    }
    
}
