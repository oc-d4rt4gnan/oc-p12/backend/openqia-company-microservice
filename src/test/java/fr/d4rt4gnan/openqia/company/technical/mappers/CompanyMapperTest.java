package fr.d4rt4gnan.openqia.company.technical.mappers;


import fr.d4rt4gnan.openqia.company.model.Company;
import fr.d4rt4gnan.openqia.company.model.CompanyBean;
import fr.d4rt4gnan.openqia.company.technical.resolvers.CompanyMapperParameterResolver;
import fr.d4rt4gnan.openqia.company.technical.utils.CompanyUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
@ExtendWith({ CompanyMapperParameterResolver.class, MockitoExtension.class })
class CompanyMapperTest {
    
    private UUID   id               = UUID.randomUUID();
    private String logoSVG          = "";
    private String firstName        = "ano";
    private String lastName         = "nymous";
    private String name             = "name";
    private String description      = "description";
    private String address          = "address";
    private String email            = "email";
    private String telephone        = "telephone";
    private String bankDetail       = "bankDetail";
    private String otherInformation = "otherInformation";
    
    @Mock
    private CompanyUtil companyUtil;
    
    @Test
    void givenCompanyInformation_whenCallingToCompanyBeanMethod_thenGettingInformationInCompanyBeanFormat (
            CompanyMapper companyMapper
    ) {
        // GIVEN
        Company company = new Company();
        company.setId(id);
        company.setLogoSVG(logoSVG);
        company.setFirstName(firstName);
        company.setLastName(lastName);
        company.setName(name);
        company.setDescription(description);
        company.setAddress(address);
        company.setEmail(email);
        company.setTelephone(telephone);
        company.setBankDetail(bankDetail);
        company.setOtherInformation(otherInformation);
        
        CompanyBean expected = new CompanyBean();
        expected.setId(id);
        expected.setLogoSVG(logoSVG);
        expected.setFirstName(firstName);
        expected.setLastName(lastName);
        expected.setName(name);
        expected.setDescription(description);
        expected.setAddress(address);
        expected.setEmail(email);
        expected.setTelephone(telephone);
        expected.setBankDetails(bankDetail);
        expected.setOtherInformation(otherInformation);
        
        // WHEN
        CompanyBean result = companyMapper.toCompanyBean(company);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenCompanyBeanInformation_whenCallingToCompanyMethod_thenGettingInformationInCompanyFormat (
            CompanyMapper companyMapper
    ) {
        // GIVEN
        CompanyBean companyBean = new CompanyBean();
        companyBean.setId(id);
        companyBean.setLogoSVG(logoSVG);
        companyBean.setFirstName(firstName);
        companyBean.setLastName(lastName);
        companyBean.setName(name);
        companyBean.setDescription(description);
        companyBean.setAddress(address);
        companyBean.setEmail(email);
        companyBean.setTelephone(telephone);
        companyBean.setBankDetails(bankDetail);
        companyBean.setOtherInformation(otherInformation);
        
        Company expected = new Company();
        expected.setId(id);
        expected.setLogoSVG(logoSVG);
        expected.setFirstName(firstName);
        expected.setLastName(lastName);
        expected.setName(name);
        expected.setDescription(description);
        expected.setAddress(address);
        expected.setEmail(email);
        expected.setTelephone(telephone);
        expected.setBankDetail(bankDetail);
        expected.setOtherInformation(otherInformation);
        
        // WHEN
        Company result = companyMapper.toCompany(companyBean);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenCompaniesBeansInformation_whenCallingToCompaniesMethod_thenGettingInformationInCompanyFormat (CompanyMapper classUnderTest) {
        // GIVEN
        CompanyBean companyBean = new CompanyBean();
        companyBean.setId(id);
        companyBean.setLogoSVG(logoSVG);
        companyBean.setFirstName(firstName);
        companyBean.setLastName(lastName);
        companyBean.setName(name);
        companyBean.setDescription(description);
        companyBean.setAddress(address);
        companyBean.setEmail(email);
        companyBean.setTelephone(telephone);
        companyBean.setBankDetails(bankDetail);
        companyBean.setOtherInformation(otherInformation);
        
        UUID id2 = UUID.randomUUID();
    
        CompanyBean companyBean2 = new CompanyBean();
        companyBean2.setId(id2);
        companyBean2.setLogoSVG(logoSVG);
        companyBean2.setFirstName(firstName);
        companyBean2.setLastName(lastName);
        companyBean2.setName(name);
        companyBean2.setDescription(description);
        companyBean2.setAddress(address);
        companyBean2.setEmail(email);
        companyBean2.setTelephone(telephone);
        companyBean2.setBankDetails(bankDetail);
        companyBean2.setOtherInformation(otherInformation);
        
        List<CompanyBean> companyBeansList = Arrays.asList(companyBean, companyBean2);
        Page<CompanyBean> clientBeans = new PageImpl<>(companyBeansList);
    
        Company company = new Company();
        company.setId(id);
        company.setLogoSVG(logoSVG);
        company.setFirstName(firstName);
        company.setLastName(lastName);
        company.setName(name);
        company.setDescription(description);
        company.setAddress(address);
        company.setEmail(email);
        company.setTelephone(telephone);
        company.setBankDetail(bankDetail);
        company.setOtherInformation(otherInformation);
    
        Company company2 = new Company();
        company2.setId(id2);
        company2.setLogoSVG(logoSVG);
        company2.setFirstName(firstName);
        company2.setLastName(lastName);
        company2.setName(name);
        company2.setDescription(description);
        company2.setAddress(address);
        company2.setEmail(email);
        company2.setTelephone(telephone);
        company2.setBankDetail(bankDetail);
        company2.setOtherInformation(otherInformation);
        
        List<Company> expected = Arrays.asList(company, company2);
        
        // WHEN
        List<Company> result = classUnderTest.toCompanies(clientBeans);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenCompanyUsableToUpdate_whenCallingUpdateCompanyBeanMethod_thenItReturnCompanyBeanUpdated (
            CompanyMapper companyMapper
    ) {
        // GIVEN
        Company company = new Company();
        company.setId(id);
        company.setLogoSVG(logoSVG);
        company.setFirstName(firstName);
        company.setLastName(lastName);
        company.setName(name);
        company.setDescription(description);
        company.setAddress(address);
        company.setEmail(email);
        company.setTelephone(telephone);
        company.setBankDetail(bankDetail);
        company.setOtherInformation("a different information");
    
        CompanyBean companyBean = new CompanyBean();
        companyBean.setId(id);
        companyBean.setLogoSVG(logoSVG);
        companyBean.setFirstName(firstName);
        companyBean.setLastName(lastName);
        companyBean.setName(name);
        companyBean.setDescription(description);
        companyBean.setAddress(address);
        companyBean.setEmail(email);
        companyBean.setTelephone(telephone);
        companyBean.setBankDetails(bankDetail);
        companyBean.setOtherInformation(otherInformation);
        
        companyMapper.setCompanyUtil(companyUtil);
        when(companyUtil.isUpdatable(any(), any())).thenReturn(true);
        
        // WHEN
        CompanyBean result = companyMapper.updateCompanyBean(company, companyBean);
        
        // THEN
        assertThat(result).isEqualTo(companyBean);
    }
    
    @Test
    void givenCompanyUnusableToUpdate_whenCallingUpdateCompanyBeanMethod_thenItReturnCompanyBean (
            CompanyMapper companyMapper
    ) {
        // GIVEN
        Company company = new Company();
        company.setId(id);
        company.setLogoSVG("");
        company.setFirstName("");
        company.setLastName("");
        company.setName("");
        company.setDescription("");
        company.setAddress("");
        company.setEmail("");
        company.setTelephone("");
        company.setBankDetail("");
        company.setOtherInformation("");
    
        CompanyBean companyBean = new CompanyBean();
        companyBean.setId(id);
        companyBean.setLogoSVG(logoSVG);
        companyBean.setFirstName(firstName);
        companyBean.setLastName(lastName);
        companyBean.setName(name);
        companyBean.setDescription(description);
        companyBean.setAddress(address);
        companyBean.setEmail(email);
        companyBean.setTelephone(telephone);
        companyBean.setBankDetails(bankDetail);
        companyBean.setOtherInformation(otherInformation);
    
        companyMapper.setCompanyUtil(companyUtil);
        when(companyUtil.isUpdatable(any(), any())).thenReturn(false);
    
        // WHEN
        CompanyBean result = companyMapper.updateCompanyBean(company, companyBean);
    
        // THEN
        assertThat(result).isEqualTo(companyBean);
    }
    
}
