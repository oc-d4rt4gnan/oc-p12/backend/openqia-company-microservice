package fr.d4rt4gnan.openqia.company.business;


import fr.d4rt4gnan.openqia.company.dao.CompanyRepository;
import fr.d4rt4gnan.openqia.company.model.Company;
import fr.d4rt4gnan.openqia.company.model.CompanyBean;
import fr.d4rt4gnan.openqia.company.technical.mappers.CompanyMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
@ExtendWith(MockitoExtension.class)
class CompanyServiceTest {
    
    @InjectMocks
    private CompanyService classUnderTest;
    
    @Mock
    private CompanyMapper     companyMapper;
    @Mock
    private CompanyRepository companyRepository;
    
    private UUID id = UUID.randomUUID();
    
    @Test
    void givenCompanyInformation_whenCallingAddCompanyMethod_thenItReturnResponse201WithTheSavedCompanyInformation() {
        // GIVEN
        CompanyBean companyBean = new CompanyBean();
        companyBean.setId(id);
    
        Company company = new Company();
        company.setId(id);
        
        when(companyMapper.toCompanyBean(any())).thenReturn(companyBean);
        when(companyRepository.save(any())).thenReturn(companyBean);
        when(companyMapper.toCompany(any())).thenReturn(company);
        
        ResponseEntity<Company> expected = new ResponseEntity<>(company, HttpStatus.CREATED);
        
        // WHEN
        ResponseEntity<Company> result = classUnderTest.addCompany(company);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAnExistingID_whenCallingGetCompanyMethod_thenItReturnTheGoodCompany() {
        // GIVEN
        CompanyBean companyBean = new CompanyBean();
        Company company = new Company();
        
        when(companyRepository.findCompanyBeanById(any())).thenReturn(java.util.Optional.of(companyBean));
        when(companyMapper.toCompany(any())).thenReturn(company);
        
        ResponseEntity<Company> expected = new ResponseEntity<>(company, HttpStatus.OK);
        
        // WHEN
        ResponseEntity<Company> result = classUnderTest.getCompany(id);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAWrongID_whenCallingGetCompanyMethod_thenItReturnAResponseNotFound() {
        // GIVEN
        when(companyRepository.findCompanyBeanById(any())).thenReturn(Optional.empty());
        
        ResponseEntity<Company> expected = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        // WHEN
        ResponseEntity<Company> result = classUnderTest.getCompany(id);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenNothing_whenCallingGetCompaniesMethod_thenItReturnAResponse200() {
        // GIVEN
        List<Company> companies = new ArrayList<>();
        
        when(companyRepository.findAll((Pageable) any())).thenReturn(Page.empty());
        when(companyMapper.toCompanies(any())).thenReturn(companies);
        
        // WHEN
        ResponseEntity<List<Company>> result = classUnderTest.getCompanies(0, 5);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
    @Test
    void givenNothing_whenCallingGetCompaniesMethod_thenItReturnAnEmptyList() {
        // GIVEN
        List<Company> expected = new ArrayList<>();
        
        when(companyRepository.findAll((Pageable) any())).thenReturn(Page.empty());
        when(companyMapper.toCompanies(any())).thenReturn(expected);
        
        // WHEN
        ResponseEntity<List<Company>> result = classUnderTest.getCompanies(0, 5);
        
        // THEN
        assertThat(result.getBody()).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenCompanyWhoExists_whenCallingUpdateCompanyMethod_thenItReturnAResponseEntityWithCode201() {
        // GIVEN
        UUID id = UUID.randomUUID();
    
        Company company = new Company();
        company.setId(id);
        
        CompanyBean companyBean = new CompanyBean();
        
        when(companyRepository.findCompanyBeanById(any())).thenReturn(Optional.of(companyBean));
        when(companyMapper.updateCompanyBean(any(), any())).thenReturn(companyBean);
        when(companyRepository.save(any())).thenReturn(companyBean);
        when(companyMapper.toCompany(any())).thenReturn(company);
        
        // WHEN
        ResponseEntity<Company> result = classUnderTest.updateCompany(id, company);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
    
    @Test
    void givenACompanyThatDoesNotExist_whenCallingUpdateCompanyMethod_thenItReturnAResponseEntityWithCode404() {
        // GIVEN
        UUID id = UUID.randomUUID();
    
        Company company = new Company();
        company.setId(id);
        
        when(companyRepository.findCompanyBeanById(any())).thenReturn(Optional.empty());
        
        // WHEN
        ResponseEntity<Company> result = classUnderTest.updateCompany(id, company);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
    
}
