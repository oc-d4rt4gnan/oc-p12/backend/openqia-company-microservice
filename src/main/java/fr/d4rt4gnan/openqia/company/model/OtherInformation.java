package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * The company's information.
 *
 * @author D4RT4GNaN
 * @since 15/01/2022
 */
public class OtherInformation extends GenericField<String> {
    
    public OtherInformation () {
        this.set("");
    }
    
    public OtherInformation (final String information) {
        this.set(information);
    }
    
}
