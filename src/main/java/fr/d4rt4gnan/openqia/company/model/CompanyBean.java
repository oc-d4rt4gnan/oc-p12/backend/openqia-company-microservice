package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.converters.*;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Objects;
import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
@Entity
@Table(name = "companies", schema = "pgcompany")
//@IdClass(CompanyBean.CompanyID.class)
public class CompanyBean {
  
  /**
   * The unique identifier of the CompanyBean object in Java and in
   * the Database.
   */
  @Id
  @Column(name = "company_id", nullable = false, updatable = false)
  private UUID id;
  
  /**
   * Company logo in SVG format.
   */
  @Column(name = "logo_svg")
  @Convert(converter = LogoSVGConverter.class)
  private LogoSVG logoSVG;
  
  /**
   * The company's user first name.
   */
  @Column(name = "user_first_name")
  @Convert(converter = FirstNameConverter.class)
  private FirstName firstName;
  
  /**
   * The company's user last name.
   */
  @Column(name = "user_last_name")
  @Convert(converter = LastNameConverter.class)
  private LastName lastName;
  
  /**
   * The company's name.
   */
  @Column(name = "company_name")
  @Convert(converter = NameConverter.class)
  private Name name;
  
  /**
   * Description of the company.
   */
  @Column(name = "description")
  @Convert(converter = DescriptionConverter.class)
  private Description description;
  
  /**
   * Company address (displayed on documents).
   */
  @Column(name = "address")
  @Convert(converter = AddressConverter.class)
  private Address address;
  
  /**
   * The phone number to contact the company.
   */
  @Column(name = "telephone", length = 31)
  @Convert(converter = TelephoneConverter.class)
  private Telephone telephone;
  
  /**
   * The mail to contact the company.
   */
  @Column(name = "email")
  @Convert(converter = EmailConverter.class)
  private Email email;
  
  /**
   * The company's bank information for payment.
   */
  @Column(name = "bank_details", length = 200)
  @Convert(converter = BankDetailsConverter.class)
  private BankDetails bankDetails;
  
  /**
   * Company information such as SIRET or insurance.
   */
  @Column(name = "other_information")
  @Convert(converter = OtherInformationConverter.class)
  private OtherInformation otherInformation;
  
  public CompanyBean () {
    //this.id               = new ID();
    this.id               = UUID.randomUUID();
    this.logoSVG          = new LogoSVG();
    this.firstName = new FirstName();
    this.lastName = new LastName();
    this.name             = new Name();
    this.description      = new Description();
    this.address          = new Address();
    this.telephone        = new Telephone();
    this.email            = new Email();
    this.bankDetails      = new BankDetails();
    this.otherInformation = new OtherInformation();
  }
  
  /**
   * Getter of the "id" attribute.
   *
   * @return the value of the "id" attribute.
   */
  /*public ID getId () {
    return id;
  }*/
  public UUID getId () {
    return id;
  }
  
  /**
   * Setter of the "id" attribute.
   *
   * @param id - The value assigned.
   */
  /*public void setId (final UUID id) {
    this.id.set(id);
  }*/
  public void setId (final UUID id) {
    this.id = id;
  }
  
  /**
   * Getter of the "logoSVG" attribute.
   *
   * @return the value of the "logoSVG" attribute.
   */
  public LogoSVG getLogoSVG () {
    return logoSVG;
  }
  
  /**
   * Setter of the "logoSVG" attribute.
   *
   * @param logo - The value assigned.
   */
  public void setLogoSVG (final String logo) {
    this.logoSVG.set(logo);
  }
  
  /**
   * Getter of the "firstName" attribute.
   *
   * @return the value of the "firstName" attribute.
   */
  public FirstName getFirstName () {
    return firstName;
  }
  
  /**
   * Setter of the "firstName" attribute.
   *
   * @param firstName - The value assigned.
   */
  public void setFirstName (final String firstName) {
    this.firstName.set(firstName);
  }
  
  /**
   * Getter of the "lastName" attribute.
   *
   * @return the value of the "lastName" attribute.
   */
  public LastName getLastName () {
    return lastName;
  }
  
  /**
   * Setter of the "lastName" attribute.
   *
   * @param lastName - The value assigned.
   */
  public void setLastName (final String lastName) {
    this.lastName.set(lastName);
  }
  
  /**
   * Getter of the "name" attribute.
   *
   * @return the value of the "name" attribute.
   */
  public Name getName () {
    return name;
  }
  
  /**
   * Setter of the "name" attribute.
   *
   * @param name - The value assigned.
   */
  public void setName (final String name) {
    this.name.set(name);
  }
  
  /**
   * Getter of the "description" attribute.
   *
   * @return the value of the "description" attribute.
   */
  public Description getDescription () {
    return description;
  }
  
  /**
   * Setter of the "description" attribute.
   *
   * @param description - The value assigned.
   */
  public void setDescription (final String description) {
    this.description.set(description);
  }
  
  /**
   * Getter of the "address" attribute.
   *
   * @return the value of the "address" attribute.
   */
  public Address getAddress () {
    return address;
  }
  
  /**
   * Setter of the "address" attribute.
   *
   * @param address - The value assigned.
   */
  public void setAddress (final String address) {
    this.address.set(address);
  }
  
  /**
   * Getter of the "telephone" attribute.
   *
   * @return the value of the "telephone" attribute.
   */
  public Telephone getTelephone () {
    return telephone;
  }
  
  /**
   * Setter of the "telephone" attribute.
   *
   * @param telephone - The value assigned.
   */
  public void setTelephone (final String telephone) {
    this.telephone.set(telephone);
  }
  
  /**
   * Getter of the "email" attribute.
   *
   * @return the value of the "email" attribute.
   */
  public Email getEmail () {
    return email;
  }
  
  /**
   * Setter of the "email" attribute.
   *
   * @param email - The value assigned.
   */
  public void setEmail (final String email) {
    this.email.set(email);
  }
  
  /**
   * Getter of the "bankDetail" attribute.
   *
   * @return the value of the "bankDetail" attribute.
   */
  public BankDetails getBankDetails () {
    return bankDetails;
  }
  
  /**
   * Setter of the "bankDetail" attribute.
   *
   * @param bankDetails - The value assigned.
   */
  public void setBankDetails (final String bankDetails) {
    this.bankDetails.set(bankDetails);
  }
  
  /**
   * Getter of the "otherInformation" attribute.
   *
   * @return the value of the "otherInformation" attribute.
   */
  public OtherInformation getOtherInformation () {
    return otherInformation;
  }
  
  /**
   * Setter of the "otherInformation" attribute.
   *
   * @param information - The value assigned.
   */
  public void setOtherInformation (final String information) {
    this.otherInformation.set(information);
  }
  
  /**
   * @inheritDoc
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CompanyBean company = (CompanyBean) o;
    return Objects.equals(this.id, company.id)
           && Objects.equals(this.logoSVG, company.logoSVG)
           && Objects.equals(this.name, company.name)
           && Objects.equals(this.description, company.description)
           && Objects.equals(this.address, company.address)
           && Objects.equals(this.telephone, company.telephone)
           && Objects.equals(this.email, company.email)
           && Objects.equals(this.bankDetails, company.bankDetails)
           && Objects.equals(this.otherInformation, company.otherInformation);
  }
  
  /**
   * @inheritDoc
   */
  @Override
  public int hashCode() {
    return Objects.hash(id, logoSVG, name, description, address, telephone, email, bankDetails, otherInformation);
  }
  
  /**
   * @inheritDoc
   */
  @Override
  public String toString() {
    return "class Company {\n"
           + "    id: "
           //+ toIndentedString(id)
           + id
           + "\n"
           + "    logoSVG: "
           + toIndentedString(logoSVG)
           + "\n"
           + "    name: "
           + toIndentedString(name)
           + "\n"
           + "    description: "
           + toIndentedString(description)
           + "\n"
           + "    address: "
           + toIndentedString(address)
           + "\n"
           + "    telephone: "
           + toIndentedString(telephone)
           + "\n"
           + "    email: "
           + toIndentedString(email)
           + "\n"
           + "    bankDetail: "
           + toIndentedString(bankDetails)
           + "\n"
           + "    otherInformation: "
           + toIndentedString(otherInformation)
           + "\n"
           + "}";
  }
  
  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   *
   * @param o - The object to be indented.
   * @return the indented string version of the object in parameter.
   */
  private String toIndentedString (final GenericField o) {
    if (o == null) {
      return "null";
    }
    return o.get().toString().replace("\n", "\n    ");
  }
  
  /**
   * Item's ID, JPA compatibility.
   *
   * @author D4RT4GNaN
   * @since 21/05/2022
   */
  static final class CompanyID implements Serializable {
    
    @Convert(converter = IDConverter.class)
    @Column(name = "company_id", nullable = false, updatable = false)
    private ID id;
    
    private void writeObject(ObjectOutputStream oos)
    throws IOException {
      oos.defaultWriteObject();
      oos.writeObject(id.get());
    }
    
    private void readObject(ObjectInputStream ois)
    throws ClassNotFoundException, IOException {
      ois.defaultReadObject();
      UUID uuid = (UUID) ois.readObject();
      ID id = new ID();
      id.set(uuid);
      this.id = id;
    }
  }
}

