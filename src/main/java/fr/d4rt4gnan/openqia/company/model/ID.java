package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import java.util.UUID;


/**
 * Company's ID.
 *
 * @author D4RT4GNaN
 * @since 15/01/2022
 */
public class ID extends GenericField<UUID> {
    
    public ID () {
        this.set(UUID.randomUUID());
    }
    
    public ID (final UUID uuid) {
        this.set(uuid);
    }
    
}
