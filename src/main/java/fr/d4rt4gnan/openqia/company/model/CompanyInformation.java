package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * The company's information.
 *
 * @author D4RT4GNaN
 * @since 15/01/2022
 */
public class CompanyInformation extends GenericField<String> {
    
    CompanyInformation () {
        this.set("");
    }
    
}
