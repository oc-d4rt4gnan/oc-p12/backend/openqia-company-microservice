package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * Company logo in SVG format.
 *
 * @author D4RT4GNaN
 * @since 13/01/2022
 */
public class LogoSVG extends GenericField<String> {
    
    public LogoSVG () {
        this.set("");
    }
    
    public LogoSVG (final String logo) {
        this.set(logo);
    }
    
}
