package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * Company user last name (displayed on documents).
 *
 * @author D4RT4GNaN
 * @since 24/05/2022
 */
public class LastName extends GenericField<String> {
    
    public LastName () {
        this.set("");
    }
    
    public LastName (final String lastName) {
        this.set(lastName);
    }
    
}
