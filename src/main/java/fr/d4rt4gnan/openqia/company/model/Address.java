package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * Company address (displayed on documents).
 *
 * @author D4RT4GNaN
 * @since 13/01/2022
 */
public class Address extends GenericField<String> {
    
    public Address() {
        this.set("");
    }
    
    public Address(final String address) {
        this.set(address);
    }
    
}
