package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * The company's contact email.
 *
 * @author D4RT4GNaN
 * @since 15/01/2022
 */
public class Email extends GenericField<String> {
    
    public Email () {
        this.set("");
    }
    
    public Email (final String email) {
        this.set(email);
    }
    
}
