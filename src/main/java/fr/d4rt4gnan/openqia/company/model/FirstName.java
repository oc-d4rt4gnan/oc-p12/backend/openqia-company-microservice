package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * Company user first name (displayed on documents).
 *
 * @author D4RT4GNaN
 * @since 24/05/2022
 */
public class FirstName extends GenericField<String> {
    
    public FirstName () {
        this.set("");
    }
    
    public FirstName (final String firstName) {
        this.set(firstName);
    }
    
}
