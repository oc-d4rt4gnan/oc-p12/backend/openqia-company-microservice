package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * The company's name.
 *
 * @author D4RT4GNaN
 * @since 13/01/2022
 */
public class Name extends GenericField<String> {
    
    public Name () {
        this.set("");
    }
    
    public Name (final String name) {
        this.set(name);
    }
    
}
