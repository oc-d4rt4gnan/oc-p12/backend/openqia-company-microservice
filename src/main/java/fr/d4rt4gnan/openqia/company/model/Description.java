package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * Description of the company.
 *
 * @author D4RT4GNaN
 * @since 13/01/2022
 */
public class Description extends GenericField<String> {
    
    public Description () {
        this.set("");
    }
    
    public Description (final String description) {
        this.set(description);
    }
    
}
