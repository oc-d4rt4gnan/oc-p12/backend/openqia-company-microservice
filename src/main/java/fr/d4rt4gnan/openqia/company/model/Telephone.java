package fr.d4rt4gnan.openqia.company.model;


import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;


/**
 * The company's contact phone number.
 *
 * @author D4RT4GNaN
 * @since 15/01/2022
 */
public class Telephone extends GenericField<String> {
    
    public Telephone () {
        this.set("");
    }
    
    public Telephone (final String telephone) {
        this.set(telephone);
    }
    
}
