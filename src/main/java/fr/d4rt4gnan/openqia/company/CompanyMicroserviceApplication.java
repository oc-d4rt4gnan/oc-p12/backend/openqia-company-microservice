package fr.d4rt4gnan.openqia.company;

import fr.d4rt4gnan.openqia.company.api.CompaniesApiController;
import org.springdoc.core.SpringDocUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyMicroserviceApplication {

	public static void main(String[] args) {
		SpringDocUtils.getConfig().addRestControllers(CompaniesApiController.class);
		SpringApplication.run(CompanyMicroserviceApplication.class, args);
	}

}
