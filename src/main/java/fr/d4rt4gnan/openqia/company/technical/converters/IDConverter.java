package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.ID;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.UUID;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class IDConverter implements AttributeConverter<ID, UUID> {
    
    @Override
    public UUID convertToDatabaseColumn (final ID id) {
        return ofNullable(id)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public ID convertToEntityAttribute (final UUID uuid) {
        return ofNullable(uuid)
                .map(ID::new)
                .orElse(null);
    }
    
}
