package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.Email;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class EmailConverter implements AttributeConverter<Email, String> {
    
    @Override
    public String convertToDatabaseColumn (final Email email) {
        return ofNullable(email)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public Email convertToEntityAttribute (final String email) {
        return ofNullable(email)
                .map(Email::new)
                .orElse(null);
    }
    
}
