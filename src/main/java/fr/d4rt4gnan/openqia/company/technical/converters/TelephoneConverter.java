package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.Telephone;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class TelephoneConverter implements AttributeConverter<Telephone, String> {
    
    @Override
    public String convertToDatabaseColumn (final Telephone telephone) {
        return ofNullable(telephone)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public Telephone convertToEntityAttribute (final String telephone) {
        return ofNullable(telephone)
                .map(Telephone::new)
                .orElse(null);
    }
    
}
