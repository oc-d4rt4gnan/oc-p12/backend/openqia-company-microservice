package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.BankDetails;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class BankDetailsConverter implements AttributeConverter<BankDetails, String> {
    
    @Override
    public String convertToDatabaseColumn (final BankDetails details) {
        return ofNullable(details)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public BankDetails convertToEntityAttribute (final String details) {
        return ofNullable(details)
                .map(BankDetails::new)
                .orElse(null);
    }
    
}
