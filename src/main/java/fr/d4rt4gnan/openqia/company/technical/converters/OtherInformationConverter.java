package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.OtherInformation;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class OtherInformationConverter implements AttributeConverter<OtherInformation, String> {
    
    @Override
    public String convertToDatabaseColumn (final OtherInformation information) {
        return ofNullable(information)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public OtherInformation convertToEntityAttribute (final String information) {
        return ofNullable(information)
                .map(OtherInformation::new)
                .orElse(null);
    }
    
}
