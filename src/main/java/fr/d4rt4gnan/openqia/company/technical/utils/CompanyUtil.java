package fr.d4rt4gnan.openqia.company.technical.utils;


/**
 * @author D4RT4GNaN
 * @since 15/01/2022
 */
public interface CompanyUtil {
    
    /**
     * Compare two data to know if the field need to be updated.
     *
     * @param inputValue - Input Data.
     * @param comparatorValue - Data in database for comparison.
     * @return true, if the value is different and not null or empty.
     */
    boolean isUpdatable (String inputValue, String comparatorValue);
    
}
