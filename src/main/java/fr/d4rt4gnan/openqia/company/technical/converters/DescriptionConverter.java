package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.Description;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class DescriptionConverter implements AttributeConverter<Description, String> {
    
    @Override
    public String convertToDatabaseColumn (final Description description) {
        return ofNullable(description)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public Description convertToEntityAttribute (final String description) {
        return ofNullable(description)
                .map(Description::new)
                .orElse(null);
    }
    
}
