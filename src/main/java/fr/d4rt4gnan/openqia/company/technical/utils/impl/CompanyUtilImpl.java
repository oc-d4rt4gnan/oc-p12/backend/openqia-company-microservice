package fr.d4rt4gnan.openqia.company.technical.utils.impl;


import fr.d4rt4gnan.openqia.company.technical.utils.CompanyUtil;

import javax.inject.Named;


/**
 * @author D4RT4GNaN
 * @since 15/01/2022
 */
@Named(value = "companyUtil")
public class CompanyUtilImpl implements CompanyUtil {
    
    /**
     * @inheritDoc
     */
    @Override
    public boolean isUpdatable (final String inputValue, final String comparatorValue) {
        return inputValue != null
            && !inputValue.isEmpty()
            && inputValue.compareTo(comparatorValue) != 0;
    }
    
}
