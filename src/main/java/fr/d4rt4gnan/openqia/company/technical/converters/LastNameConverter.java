package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.LastName;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class LastNameConverter implements AttributeConverter<LastName, String> {
    
    @Override
    public String convertToDatabaseColumn (final LastName lastName) {
        return ofNullable(lastName)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public LastName convertToEntityAttribute (final String lastName) {
        return ofNullable(lastName)
                .map(LastName::new)
                .orElse(null);
    }
    
}
