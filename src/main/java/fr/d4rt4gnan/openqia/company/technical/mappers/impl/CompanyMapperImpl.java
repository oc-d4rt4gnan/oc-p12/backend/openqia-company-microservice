package fr.d4rt4gnan.openqia.company.technical.mappers.impl;


import fr.d4rt4gnan.openqia.company.model.Company;
import fr.d4rt4gnan.openqia.company.model.CompanyBean;
import fr.d4rt4gnan.openqia.company.technical.mappers.CompanyMapper;
import fr.d4rt4gnan.openqia.company.technical.utils.CompanyUtil;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;
import org.springframework.data.domain.Page;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
@Named(value = "companyMapper")
public class CompanyMapperImpl implements CompanyMapper {
    
    /**
     * Utility for Company objects.
     */
    private CompanyUtil companyUtil;
    
    /**
     *
     * @param value
     */
    @Inject
    public void setCompanyUtil (final CompanyUtil value) {
        this.companyUtil = value;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public Company toCompany (final CompanyBean companyBean) {
        Company company = new Company();
        
        //company.setId(companyBean.getId().get());
        company.setId(companyBean.getId());
        company.setLogoSVG(companyBean.getLogoSVG().get());
        company.setFirstName(companyBean.getFirstName().get());
        company.setLastName(companyBean.getLastName().get());
        company.setName(companyBean.getName().get());
        company.setDescription(companyBean.getDescription().get());
        company.setAddress(companyBean.getAddress().get());
        company.setEmail(companyBean.getEmail().get());
        company.setTelephone(companyBean.getTelephone().get());
        company.setBankDetail(companyBean.getBankDetails().get());
        company.setOtherInformation(companyBean.getOtherInformation().get());
        
        return company;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public CompanyBean toCompanyBean (final Company company) {
        CompanyBean companyBean = new CompanyBean();
    
        companyBean.setId(company.getId());
        companyBean.setLogoSVG(company.getLogoSVG());
        companyBean.setFirstName(company.getFirstName());
        companyBean.setLastName(company.getLastName());
        companyBean.setName(company.getName());
        companyBean.setDescription(company.getDescription());
        companyBean.setAddress(company.getAddress());
        companyBean.setEmail(company.getEmail());
        companyBean.setTelephone(company.getTelephone());
        companyBean.setBankDetails(company.getBankDetail());
        companyBean.setOtherInformation(company.getOtherInformation());
        
        return companyBean;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public List<Company> toCompanies (final Page<CompanyBean> companiesBeans) {
        List<CompanyBean> companyBeanList = companiesBeans.toList();
        return companyBeanList.stream().map(this::toCompany).collect(Collectors.toList());
    }
    
    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public CompanyBean updateCompanyBean (
            final Company company,
            final CompanyBean companyBean
    ) {
        Map<String, GenericField> mapper = new HashMap<>();
        mapper.put(company.getFirstName(), companyBean.getFirstName());
        mapper.put(company.getLastName(), companyBean.getLastName());
        mapper.put(company.getName(), companyBean.getName());
        mapper.put(company.getLogoSVG(), companyBean.getLogoSVG());
        mapper.put(company.getDescription(), companyBean.getDescription());
        mapper.put(company.getAddress(), companyBean.getAddress());
        mapper.put(company.getTelephone(), companyBean.getTelephone());
        mapper.put(company.getEmail(), companyBean.getEmail());
        mapper.put(company.getBankDetail(), companyBean.getBankDetails());
        mapper.put(company.getOtherInformation(), companyBean.getOtherInformation());
        
        mapper.forEach((companyValue,companyBeanField) -> {
            if (companyBeanField.get() instanceof String
                && Boolean.TRUE.equals(
                    companyUtil.isUpdatable(companyValue, (String) companyBeanField.get())
                )
            ) {
                companyBeanField.set(companyValue);
            }
        });
        
        return companyBean;
    }
}
