package fr.d4rt4gnan.openqia.company.technical.filters;


import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Cors filter allowing cross-domain requests
 *
 * @author d4rt4gnan
 * @since 23/05/2022
 */
@Component
public class CorsFilter implements Filter {
    
    public void doFilter (ServletRequest req, ServletResponse res, FilterChain chain)
    throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PATCH, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers",
                           "Content-Type, Authorization, Content-Length, X-Requested-With");
        chain.doFilter(req, res);
    }
    
}
