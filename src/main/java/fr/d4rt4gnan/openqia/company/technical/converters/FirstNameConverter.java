package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.FirstName;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class FirstNameConverter implements AttributeConverter<FirstName, String> {
    
    @Override
    public String convertToDatabaseColumn (final FirstName firstName) {
        return ofNullable(firstName)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public FirstName convertToEntityAttribute (final String firstName) {
        return ofNullable(firstName)
                .map(FirstName::new)
                .orElse(null);
    }
    
}
