package fr.d4rt4gnan.openqia.company.technical.utils;


/**
 * @author D4RT4GNaN
 * @since 13/01/2022
 * @param <T> - specific implementation
 */
public abstract class GenericField<T> {
    
    /**
     * The value of the object.
     */
    private T value;
    
    /**
     * Value recovery method.
     *
     * @return the value.
     */
    public T get() {
        return value;
    }
    
    /**
     * Method of changing the value.
     *
     * @param value - The value assigned.
     */
    public void set (T value) {
        this.value = value;
    }
    
}
