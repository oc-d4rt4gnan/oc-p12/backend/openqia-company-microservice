package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.Address;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class AddressConverter implements AttributeConverter<Address, String> {
    
    @Override
    public String convertToDatabaseColumn (final Address address) {
        return ofNullable(address)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public Address convertToEntityAttribute (final String address) {
        return ofNullable(address)
                .map(Address::new)
                .orElse(null);
    }
    
}
