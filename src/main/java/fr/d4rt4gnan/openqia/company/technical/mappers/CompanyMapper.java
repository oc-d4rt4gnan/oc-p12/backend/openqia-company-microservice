package fr.d4rt4gnan.openqia.company.technical.mappers;


import fr.d4rt4gnan.openqia.company.model.Company;
import fr.d4rt4gnan.openqia.company.model.CompanyBean;
import fr.d4rt4gnan.openqia.company.technical.utils.CompanyUtil;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
public interface CompanyMapper {
    
    /**
     * Allows to convert the Company object from the database version to
     * the transfer version via the swagger.
     *
     * @param companyBean
     *         - The Client object in database format.
     *
     * @return the Company object in transfer format via swagger.
     */
    Company toCompany (CompanyBean companyBean);
    
    
    
    /**
     * Convert the Company object from the swagger version to the database
     * version.
     *
     * @param company
     *         - The Company object in transfer format via swagger.
     *
     * @return the Company object in database format.
     */
    CompanyBean toCompanyBean (Company company);
    
    /**
     * Allows you to convert Companies objects from the database version to
     * the transfer version via the swagger.
     *
     * @param companiesBeans - The Companies object list of the database version.
     * @return the Client object list in transfer format via swagger.
     */
    List<Company> toCompanies (Page<CompanyBean> companiesBeans);
    
    /**
     * Update only the different fields and not null.
     *
     * @param company
     *         - Input data.
     * @param companyBean
     *         - Data in database for comparison.
     *
     * @return a company object in database format with updated fields.
     */
    CompanyBean updateCompanyBean (Company company, CompanyBean companyBean);
    
    /**
     * Setter of the "companyUtil" attribute for the dependency
     * injection.
     *
     * @param value
     *         - The value assigned.
     */
    void setCompanyUtil (CompanyUtil value);
    
}
