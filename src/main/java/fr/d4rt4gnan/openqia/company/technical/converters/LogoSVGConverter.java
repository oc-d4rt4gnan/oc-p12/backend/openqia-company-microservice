package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.LogoSVG;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class LogoSVGConverter implements AttributeConverter<LogoSVG, String> {
    
    @Override
    public String convertToDatabaseColumn (final LogoSVG logo) {
        return ofNullable(logo)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public LogoSVG convertToEntityAttribute (final String logo) {
        return ofNullable(logo)
                .map(LogoSVG::new)
                .orElse(null);
    }
    
}
