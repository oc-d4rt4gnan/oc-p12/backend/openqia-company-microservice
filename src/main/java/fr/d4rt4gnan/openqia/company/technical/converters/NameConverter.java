package fr.d4rt4gnan.openqia.company.technical.converters;


import fr.d4rt4gnan.openqia.company.model.Name;
import fr.d4rt4gnan.openqia.company.technical.utils.GenericField;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class NameConverter implements AttributeConverter<Name, String> {
    
    @Override
    public String convertToDatabaseColumn (final Name name) {
        return ofNullable(name)
                .map(GenericField::get)
                .orElse(null);
    }
    
    @Override
    public Name convertToEntityAttribute (final String name) {
        return ofNullable(name)
                .map(Name::new)
                .orElse(null);
    }
    
}
