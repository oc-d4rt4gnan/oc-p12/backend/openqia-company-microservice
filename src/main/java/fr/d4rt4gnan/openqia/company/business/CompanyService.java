package fr.d4rt4gnan.openqia.company.business;


import fr.d4rt4gnan.openqia.company.api.CompaniesApiDelegate;
import fr.d4rt4gnan.openqia.company.dao.CompanyRepository;
import fr.d4rt4gnan.openqia.company.model.Company;
import fr.d4rt4gnan.openqia.company.model.CompanyBean;
import fr.d4rt4gnan.openqia.company.model.Icon;
import fr.d4rt4gnan.openqia.company.technical.mappers.CompanyMapper;
import fr.d4rt4gnan.openqia.company.technical.utils.impl.ImageTracer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
@RestController
@Service
public class CompanyService implements CompaniesApiDelegate {
    
    /**
     * CompanyRepository dependency.
     */
    private CompanyRepository companyRepository;
    
    /**
     * CompanyMapper dependency.
     */
    private CompanyMapper companyMapper;
    
    /**
     * Injection of CompanyMapper dependencies into the service.
     *
     * @param value - The CompanyMapper to inject.
     */
    @Inject
    public void setCompanyMapper (CompanyMapper value) {
        this.companyMapper = value;
    }
    
    /**
     * Injection of CompanyRepository dependencies into the service.
     *
     * @param value - The CompanyRepository to inject.
     */
    @Inject
    public void setCompanyRepository (CompanyRepository value) {
        this.companyRepository = value;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Company> addCompany (
            final Company company
    ) {
        CompanyBean companyBean = companyMapper.toCompanyBean(company);
        CompanyBean companyBeanSaved = companyRepository.save(companyBean);
        return new ResponseEntity<>(companyMapper.toCompany(companyBeanSaved), HttpStatus.CREATED);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Company> getCompany (
            final UUID id
    ) {
        Optional<CompanyBean> companyDB = companyRepository.findCompanyBeanById(id);
        return companyDB.map(companyBean -> new ResponseEntity<>(companyMapper.toCompany(companyBean), HttpStatus.OK))
                       .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<List<Company>> getCompanies(
            final Integer pageNumber,
            final Integer pageSize
    ) {
        Pageable pageRequest = PageRequest.of(pageNumber, pageSize);
        Page<CompanyBean> companyBeans = companyRepository.findAll(pageRequest);
        List<Company> companies = companyMapper.toCompanies(companyBeans);
        return new ResponseEntity<>(companies, HttpStatus.OK);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Company> updateCompany(
            UUID id,
            Company company
    ) {
        Optional<CompanyBean> optionalCompanyBean = companyRepository.findCompanyBeanById(id);
        if (optionalCompanyBean.isPresent()) {
            CompanyBean companyBean = companyMapper.updateCompanyBean(company, optionalCompanyBean.get());
            CompanyBean companyBeanUpdated = companyRepository.save(companyBean);
            return new ResponseEntity<>(companyMapper.toCompany(companyBeanUpdated), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @Override
    public ResponseEntity<Icon> convertIcon (String body) {
        String base64Image = body.split(",")[1];
        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
        try {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
            String svgAsString = ImageTracer.imageToSVG(img, setOptions(), null);
            Icon icon = new Icon();
            icon.setValue(svgAsString);
            return new ResponseEntity<>(icon, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    private HashMap<String, Float> setOptions() {
        // Options
        HashMap<String,Float> options = new HashMap<>();

        // Tracing
        options.put("ltres",1f);
        options.put("qtres",1f);
        options.put("pathomit",8f);

        // Color quantization
        options.put("colorsampling",1f); // 1f means true ; 0f means false: starting with generated palette
        options.put("numberofcolors",16f);
        options.put("mincolorratio",0.02f);
        options.put("colorquantcycles",3f);

        // SVG rendering
        options.put("scale",1f);
        options.put("roundcoords",0f); // 1f means rounded to 1 decimal places, like 7.3 ; 3f means rounded to 3
        // places, like 7.356 ; etc.
        options.put("lcpr",0f);
        options.put("qcpr",0f);
        options.put("desc",1f); // 1f means true ; 0f means false: SVG descriptions deactivated
        options.put("viewbox",1f); // 1f means true ; 0f means false: fixed width and height

        // Selective Gauss Blur
        options.put("blurradius",0f); // 0f means deactivated; 1f .. 5f : blur with this radius
        options.put("blurdelta",20f); // smaller than this RGB difference will be blurred
    
        return options;
    }
}