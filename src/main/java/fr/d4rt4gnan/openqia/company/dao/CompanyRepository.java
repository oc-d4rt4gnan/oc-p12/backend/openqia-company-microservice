package fr.d4rt4gnan.openqia.company.dao;


import fr.d4rt4gnan.openqia.company.model.CompanyBean;
import fr.d4rt4gnan.openqia.company.model.ID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 10/01/2022
 */
@Repository
public interface CompanyRepository extends PagingAndSortingRepository<CompanyBean, UUID> {
    
    /**
     * Getter of an optional CompanyBean by its ID.
     *
     * @param id - The uuid of the CompanyBean in search.
     * @return an optional with the CompanyBean found if there is it.
     */
    Optional<CompanyBean> findCompanyBeanById (UUID id);
    
}