CREATE SCHEMA IF NOT EXISTS pgcompanies AUTHORIZATION postgres;

SET search_path TO extensions, pgcompanies;

CREATE TABLE IF NOT EXISTS pgcompanies.companies
(
    id uuid DEFAULT uuid_generate_v4(),
    logo_svg VARCHAR,
    name VARCHAR NOT NULL,
    description VARCHAR,
    address VARCHAR,
    phone_number VARCHAR(31),
    mail VARCHAR,
    bank_details VARCHAR(200),
    other_informations VARCHAR,
    PRIMARY KEY (id)
);

COMMENT ON TABLE pgcompanies.companies IS 'Contains all informations about companies.';
COMMENT ON COLUMN pgcompanies.companies.id IS 'The table''s arbitrary primary key.';
COMMENT ON COLUMN pgcompanies.companies.logo_svg IS 'The company logo in SVG format.';
COMMENT ON COLUMN pgcompanies.companies.name IS 'The company name.';
COMMENT ON COLUMN pgcompanies.companies.description IS 'The short description of the company.';
COMMENT ON COLUMN pgcompanies.companies.address IS 'The company address.';
COMMENT ON COLUMN pgcompanies.companies.phone_number IS 'The company phone number to contact it.';
COMMENT ON COLUMN pgcompanies.companies.mail IS 'The company mail to contact it.';
COMMENT ON COLUMN pgcompanies.companies.bank_details IS 'The company''s banking information used for payments.';
COMMENT ON COLUMN pgcompanies.companies.other_informations IS 'Additional company information (e.g. SIRET).';