## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Prerequisites](#prerequisites)
4. [Installation](#installation)
5. [Configuration](#configuration)
6. [Usage](#usage)
### General Info
***
Microservice for managing companies available in the webapp.

It allows you to update company's information.

It is part of a set of Microservices forming the OpenQIA project.

The code is evolving, it is currently in its first version.
## Technologies
***
A list of technologies used within the project:
* [Spring Framework](https://spring.io/projects/spring-framework): Version 2.6.1 
* [Maven](https://maven.apache.org): Version 3.6.0
* [Git](https://git-scm.com): Version 2.7.4
* [OpenAPI](https://www.openapis.org): Version 3.0.3
* [Java](https://www.java.com): Version 1.8.0_201
## Prerequisites
You need a database (here PostgreSQL is used for development) of your choice.

The database creation schema can be found in the **src/main/resources/sql/create/create_company_schema_db.sql** file.

The datasets (if necessary) are in the src/main/resources/sql/insert/dataset.sql file.
## Installation
***
For the installation, it is enough to clone the project 
```
$ git clone https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-company-microservice.git
```
as well as the other necessary microservices (in particular the **configuration server**):
* [Configuration Server](https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-config-server-microservice)
* [Documents Microservice](https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-documents-microservice)
* [Items Microservice](https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-items-microservice)
Then, launch the configuration server and, either to launch the project directly from an IDE thanks to **spring boot**, or to compile with **Maven**
```
$ cd /path_to_file/openqia.company

$ mvn clean install
```
and to put the **jar** created form target folder into **tomcat server**. 
## Configuration
***
For the configuration, it must be done in the properties file of the remote repo.

You can find the instructions [here](https://gitlab.com/oc-d4rt4gnan/oc-p12/backend/openqia-config-server-microservice).
## Usage
***
You can use the [Front end](https://gitlab.com/oc-d4rt4gnan/oc-p12/frontend) associated or create your own front end by using the yaml in **src/main/resources** folder.

